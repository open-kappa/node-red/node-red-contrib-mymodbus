![MyModbus logo](mymodbus.png)

This package provides node-red nodes for managing the modbus protocol.
It supports *typescript*.

## Links

* Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/node-red/node-red-contrib-mymodbus)

* Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/node-red/node-red-contrib-mymodbus)

* List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

## License

*@open-kappa/node-red-contrib-mymodbus* is released under the liberal MIT
License. Please refer to the LICENSE.md project file for further details.

## Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
