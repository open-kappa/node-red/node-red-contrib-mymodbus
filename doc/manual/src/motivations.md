# MOTIVATIONS

This simple library provides some nodes to interact with the modbus protocol.
It has been created because there are already some available alternatives, but
they do not fit into my production standard:

* Privide all functionalities, with fewer code:
    * So maintainers must deal with less nodes/code
    * So we follow the UNIX philosophy: many features can be replaced by
        combining standard node-red nodes
    * So the runtime is lighter/more performant
* Use typescript:
    * Maintainance/contributing is simpler
* Be error firendly:
    * So users can choose what to do, and node-red never crashes
