/* eslint-disable */

/**
 * @module -- Manual --
 */

export class Manual
{
    private constructor()
    {}

    /**
     * [[include:motivations.md]]
     */
    "1. Motivations": void;

    /**
     * [[include:contributing.md]]
     */
    "3. Contributing": void;

    /**
     * [[include:copyright.md]]
     */
    "4. Copyright": void;

    /**
     * [[include:7.changelog.md]]
     */
    "5. Changelog": void;
}
